﻿using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace LearnSchool.Components
{
    public class ByteImageConverter
    {
        public static ImageSource ByteToImage(byte[] imageData)
        {
            var biImg = new BitmapImage();
            var ms = new MemoryStream(imageData);
            biImg.BeginInit();
            biImg.StreamSource = ms;
            biImg.EndInit();

            return biImg;
        }
    }
}