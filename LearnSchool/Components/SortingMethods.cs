﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LearnSchool.Components
{
    public static class SortingMethods
    {
        public static List<string> Methods = new List<string>()
        {
            "Сортировка отключена",
            "По возрастанию",
            "По убыванию"
        };
    }
}
