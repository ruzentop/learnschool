﻿namespace LearnSchool.DB
{
    public partial class ServicePhoto
    {
        public string ImagePath => $@"\{PhotoPath}";
    }
}
