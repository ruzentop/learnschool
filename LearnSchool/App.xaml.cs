﻿using System.Windows;
using LearnSchool.DB;

namespace LearnSchool
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        private static readonly SchoolEntities _connection = new SchoolEntities();
        public static SchoolEntities Connection => _connection;
        public static bool IsAdministratorMode { get; set; } = false;
        public static Visibility AdminVisibility => IsAdministratorMode ? Visibility.Visible : Visibility.Collapsed;
    }
}